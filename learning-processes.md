# Learning Processes

### 1.What is the Feynman Technique? Explain in 1 line.
- The Feynman Technique is a learning method that involves explaining a concept in simple terms as if teaching it to a beginner to ensure deep understanding.

### 2.In this video, what was the most interesting story or idea for you?
- The coolest thing was learning how our brains can change and adapt through experiences and learning.

### 3.What are active and diffused modes of thinking?
- Active mode is focused, concentrated thinking, while diffused mode is a more relaxed, subconscious thinking state.

### 4.According to the video, what are the steps to take when approaching a new topic? Only mention the points.
The steps mentioned in the video are:
- Set a goal.
- Break it down.
- Search for resources.
- Create a roadmap.

### 5.What are some of the actions you can take going forward to improve your learning process?
Some actions to improve learning include: 
- Setting clear goals
- Embracing challenges
- Practicing regularly 
- Seeking feedback 
- Staying curious.