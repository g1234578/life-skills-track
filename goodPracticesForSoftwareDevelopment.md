# Good Practices for Software Development

### Question 1: Which point(s) were new to you?

- Using frequent feedback to clarify vague requirements.
- Clearly framing questions with problems and attempted solutions.
- Joining meetings early to chat with team members.
- Learning and applying deep work techniques.

### Question 2: Which area do you think you need to improve on? What are your ideas to make progress in that area?

**Area to improve**: Focusing 100% on tasks.

**Ideas for progress**:
- Schedule dedicated work time without interruptions.
- Silence phone notifications and block social media during work.
- Use time-tracking apps like Boosted.
- Eat balanced meals and exercise regularly for energy.
