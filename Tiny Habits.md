# Tiny Habits

## 1. Tiny Habits - BJ Fogg

### Question 1
**In this video, what was the most interesting story or idea for you?**

**Answer:**
- The most interesting story in BJ Fogg's video is about his friend starting the habit of flossing by initially flossing just one tooth. This small, easy action made it simple to start and eventually led to flossing all teeth regularly. This shows how tiny habits can grow into bigger ones.

## 2. Tiny Habits by BJ Fogg - Core Message

### Question 2
**How can you use B = MAP to make making new habits easier? What are M, A, and P?**

**Answer:**
- To make new habits easier using BJ Fogg's B = MAP formula:
  - M (Motivation): Find a personal reason to do the habit.
  - A (Ability): Make the habit simple and easy.
  - P (Prompt): Use reminders to trigger the habit.
  - For example, to start exercising, choose a fun activity (Motivation), start with one push-up (Ability), and do it after brushing your teeth (Prompt). This combination makes it easier to build new habits.

### Question 3
**Why is it important to "Shine" or celebrate after each successful completion of a habit?**

**Answer:**
- Celebrating after completing a habit is important because it creates a positive emotion that your brain associates with the habit, making you want to repeat it. This positive reinforcement helps to quickly establish the habit by making it enjoyable and rewarding.

## 3. 1% Better Every Day Video

### Question 4
**In this video, what was the most interesting story or idea for you?**

**Answer:**
The most interesting idea from the video is that making small improvements in your habits every day can lead to big success over time. This means focusing on tiny changes, like using a better pillow for sleep or practicing a skill for just a few minutes each day. These small improvements add up, like how a cycling team won the Tour de France by making many small changes. The video also talks about how our habits shape our identity, and by changing our habits, we can change who we are and what we believe about ourselves.

## 4. Book Summary of Atomic Habits

### Question 5
**What is the book's perspective about identity?**

**Answer:**
- Your identity shapes your habits; focus on becoming the type of person who embodies the habits you want.

### Question 6
**Write about the book's perspective on how to make a habit easier to do.**

**Answer:**
- Simplify the process and reduce friction to make the habit easy to start and continue.

### Question 7
**Write about the book's perspective on how to make a habit harder to do.**

**Answer:**
- Increase the friction and obstacles to make the unwanted habit more difficult to perform.

## 5. Reflection

### Question 8
**Pick one habit that you would like to do more of. What are the steps that you can take to make the cue obvious or the habit more attractive or easy and/or the response satisfying?**

**Answer:**
- **Habit:** Exercising daily.
  - Make the cue obvious: Place workout clothes and equipment in a visible spot.
  - Make the habit attractive: Join a fun exercise class or listen to favorite music while exercising.
  - Make it easy: Start with short, manageable workouts.
  - Make the response satisfying: Track progress and reward yourself with a healthy treat or relaxation time.

### Question 9
**Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make the cue invisible or the process unattractive or hard or the response unsatisfying?**

**Answer:**
- **Habit:** Reducing screen time before bed.
  - Make the cue invisible: Keep electronic devices out of the bedroom.
  - Make the process unattractive: Use apps that limit screen time or enable grayscale mode.
  - Make it hard: Set a cut-off time for device usage and stick to it.
  - Make the response unsatisfying: Reflect on the negative effects of excessive screen time on sleep quality.
