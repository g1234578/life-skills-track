# Grit and Growth Mindset

## 1. Grit
- **Question**: Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.
- **Summary**: The video discusses the concept of grit, emphasizing perseverance and passion for long-term goals.

## 2. Introduction to Growth Mindset
- **Question**: Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
- **Summary**: This video introduces the concept of growth mindset, highlighting the belief that abilities can be developed through dedication and hard work.

## 3. Understanding Internal Locus of Control
- **Question**: What is the Internal Locus of Control? What is the key point in the video?
- **Internal Locus of Control**: It refers to the belief that one has control over their own life and outcomes. The key point in the video is the importance of cultivating an internal locus of control for staying motivated and achieving success.

## 4. How to build a Growth Mindset
- **Question**: What are the key points mentioned by the speaker to build growth mindset (explanation not needed).
- **Key Points**:
  1. Embrace Challenges
  2. Persist in the Face of Setbacks
  3. See Effort as the Path to Mastery
  4. Learn from Criticism
  5. Find Lessons and Inspiration in the Success of Others

## 5. Mindset - A MountBlue Warrior Reference Manual
- **Question**: What are your ideas to take action and build Growth Mindset?
- **Ideas to Build Growth Mindset**:
  - Continuously learn new skills and technologies.
  - Embrace challenges as opportunities for growth.
  - Seek feedback and use it constructively.
  - Celebrate progress and milestones.
  - Surround yourself with growth-oriented peers.
  - Learn from setbacks and failures.
  - Set ambitious yet achievable goals.
  - Stay curious and open-minded.
  - Practice self-compassion and avoid self-criticism.
  - Reflect on your growth journey regularly.
