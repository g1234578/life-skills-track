# Listening and Active Communications

### 1.What are the steps/strategies to do Active Listening? 
- Focus: Give undivided attention to the speaker, maintaining eye contact and avoiding distractions.
  
- Engage: Use verbal and non-verbal cues to show interest and engagement, such as nodding and open body language.

- Summarize: Paraphrase the speaker's words to ensure understanding and demonstrate empathy.

- Clarify : Ask questions to clarify understanding and encourage further explanation.

- Acknowledge Emotions : Reflect the speaker's emotions to validate their feelings and show empathy.

- Provide Feedback: Offer constructive responses to actively engage with the speaker's message and foster meaningful dialogue.

### 2.According to Fisher's model, what are the key points of Reflective Listening?
According to Fisher's model, the key points of Reflective Listening are:

- Pay Full Attention : Listen carefully without interruptions.
- Understand the Message : Try to grasp the speaker's words and feelings.
- Reflect Back : Repeat or paraphrase what the speaker said to show understanding.
- Acknowledge Emotions : Recognize and validate the speaker's feelings.
- Avoid Judgement : Listen without criticizing or judging.
- Clarify : Ask questions if something is unclear to ensure full understanding.

### 3. What are the obstacles in your listening process?
- Distractions: Environmental noise and interruptions.
- Prejudgments : Forming opinions before listening fully.
- Mind Wandering: Thinking about unrelated matters.
- Emotional Reactions : Strong feelings that block understanding.
- Impatience: Wanting to speak instead of listening.
- Multitasking : Trying to do other tasks while listening.

### 4. What can you do to improve your listening?
- Eliminate Distractions : Find a quiet environment and put away devices.
- Stay Focused : Concentrate fully on the speaker.
- Avoid Prejudgment : Keep an open mind and listen completely.
- Practice Patience : Allow the speaker to finish before responding.
- Show Engagement : Use nods and verbal cues to show you're listening.
- Reflect and Clarify : Paraphrase and ask questions to ensure understanding.

### 5. When do you switch to Passive communication style in your day to day life?
- I switch to a passive communication style in my day-to-day life when I want to avoid conflict, feel intimidated, lack confidence, prioritize harmony, feel overwhelmed, or have little interest in the conversation.

### 6. When do you switch into Aggressive communication styles in your day to day life?
- I switch to an aggressive communication style in my day-to-day life when asserting needs, expressing frustration, setting boundaries, defending beliefs, taking charge, or expressing urgency.

### 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
- I switch to passive-aggressive communication styles in my day-to-day life when avoiding direct confrontation, expressing resentment, feeling powerless, seeking attention, manipulating others, or coping with stress.

### 8. How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)
To make communication assertive:
- Practice active listening.
- Use "I" statements.
- Express needs and boundaries clearly.
- Stay calm and respectful.
- Seek compromise.
- Use assertive body language.